<!DOCTYPE html>
<html>
<head>
	<title>INTRO TO PHP</title>
	<link rel="icon" href="favicon.png" type="image/gif" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/bootstrap.css">
	<!-- CSS -->
	<style type="text/css">
		*{
			margin: 0;
			padding: 0;
			box-sizing: border-box;
		}
		h1{
			text-align: center;
			padding: 10px;
		}
	</style>
</head>
<body>
	<h1>INTRO TO PHP</h1>

	<?php
		// $userName = "USD";
		// $age = 27;
		// $pi = 3.14;
		// $single = true;
		// echo "Hi I'm $userName! $age PESOS <br>";
		// echo "The value of pi is $pi <br>";	
	?>

	<!-- Operators -->
	<?php
		// $num1 = 20;
		// $num2 = 5;
		// $sum = $num1 + $num2;
		// $remainder = $num1 % $num2;
		// echo "The sum is $sum <br>";
		// echo  ++$remainder;
		// echo  $remainder;
	?>

	<!-- Control Structure -->
	<?php 
		// $num1 = 2;
		// $num2 = 17;

		// if($num1 > $num2){
		// 	echo "$num1 is greater than $num2 <br>";
		// }else{
		// 	echo "$num2 is greater than $num1 <br>";
		// }

		#Switch case
		// $today = "Thursday";
		// switch($today){
		// 	case "Monday":
		// 	#Do this
		// 	echo "I love mondays";
		// 	break;
		// 	case "Thursday":
		// 	#Do this
		// 	echo "One more day";
		// 	break;
		// 	case "Sunday":
		// 	echo "Pray for us sinners";
		// 	break;
		// 	default:
		// 	#Do this
		// 	echo "May pasok pa?";
		// 	break;
		// }

	?>

	<?php
		// $students = ["ona","pochi","archie"];
		

		// echo "Hello $students[0] <br>";

		// foreach($students as $student){
		// 	echo "$student <br>";
		// };
		// $grades = [
		// 	"math" => 80,
		// 	"english" => 96,
		// 	"chemistry" => 91
		// ];
		// // var_dump($grades);
		// // die();

		// foreach ($grades as $key => $value) {
		// 	echo "The $key is $value <br>";

		// }
	?>

	<!-- Activity -->
	<?php
		$months = [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"
		];
		
			$seasons = [
			"Winter", //Jan - March
			"Fall",	  //April-June
			"Summer", //July-September
			"Spring", //October-December
			
			];


		
		foreach($months as $month){
			if($month === "January" ||$month === "February" ||$month === "March"){
				echo "The Month is $month and the season is season $seasons[0] <br>";
			}
			elseif ($month === "April" || $month === "May" || $month === "June") {
				echo "The Month is $month and the season is season $seasons[1] <br>";
			}
			elseif ($month === "April" || $month ===  "May" || $month==="June") {
				echo "The Month is $month and the season is season $seasons[2] <br>";;
			}
			elseif ($month === "April" || $month === "May" || $mont ==="June") {
				echo "The Month is $month and the season is season $seasons[3] <br>";;
			}
				

		}


	
	?>

	<!-- Script -->
	<script type="text/javascript">
		let time = new Date();
	</script>
</body>
</html>