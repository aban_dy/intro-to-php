<?php
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];

	#validation
	session_start();
	if(strlen($firstName) === 0 || strlen($lastName) === 0){
		$_SESSION['errorMsg'] = "Pleas fill up the form properly";
		header("Location: " . $_SERVER["HTTP_REFERER"]);


	}else{
		#Session
		$_SESSION['firstName'] = $firstName;
		$_SESSION['lastName'] = $lastName;

		#Pass to landing-page.php
		#use header();
		header("Location:../views/landing-page.php");
	}
?>

