<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="favicon.png" type="image/gif" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/bootstrap.css">
	<title>MOVIES</title>
</head>
<body class="text-uppercase">
	<h1 class="text-center text-uppercase p-3">movies</h1>
	<?php
		$movies =[
			[
				"title" => "Titanic",
				"year" => 1997,
				"director" => "James Cameron",
				"genre" => "Disaster"
			],
			[
				"title" => "Catch me if you can",
				"year" => 2001,
				"director" => "Rancel Po",
				"genre" => "Comedy"
			],
			[
				"title" => "American Pie",
				"year" => 2000,
				"director" => "Yong Dy",
				"genre" => "Horror"
			],
			[
				"title" => "Titanic",
				"year" => 1997,
				"director" => "James Cameron",
				"genre" => "Disaster"
			],
			[
				"title" => "Catch me if you can",
				"year" => 2001,
				"director" => "Rancel Po",
				"genre" => "Comedy"
			],
			[
				"title" => "American Pie",
				"year" => 2000,
				"director" => "Yong Dy",
				"genre" => "Horror"
			]
		];
	?>
	<div class="container">
		<div class="row">
			<?php
				foreach($movies as $movie){
			?>
			<div class="card w-25 col-4 mx-auto my-2 bg-light">
				<h3 class="text-center">Movie</h3>
				<p>Title: <?php echo $movie["title"]?></p>
				<p>Year: <?php echo $movie["year"]?></p>
				<p>Year: <?php echo $movie["director"]?></p>
				<p>Year: <?php echo $movie["genre"]?></p>
			</div>
			<?php
				}
			?>
		</div>
	</div>
</body>
</html>